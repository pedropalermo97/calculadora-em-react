import React, {useState} from 'react'
import Display from '../Display/Display'
import Button from '../Button/Button'

import './Calculator.css'

function Calculator() {

    const [pastNumber, setpastNumber] = useState("")
    const [newNumber, setnewNumber] = useState("")
    const [currentNumber, setcurrentNumber] = useState("")

    const [dotPressed, setdotPressed] = useState(false)
    const [operator, setOperator] = useState("")
    const [operatorPressed, setoperatorPressed] = useState(false)

    function clearDisplay(){
        setpastNumber("")
        setnewNumber("")
        setcurrentNumber("")
        setOperator("")
        setoperatorPressed(false)
        setdotPressed(false)
    }

    function addNumber(number){
        if(!operatorPressed) {
            setcurrentNumber(currentNumber + number)
            setpastNumber(pastNumber + number)
            console.log(pastNumber)
        }else{
            setnewNumber(newNumber + number)
            setcurrentNumber(currentNumber + number)
        }
    }

    function addDot(){
        if(currentNumber !== ""){
            if(!dotPressed && !operatorPressed) {
                setcurrentNumber(currentNumber+ ".")
                setpastNumber(pastNumber+ ".")
                setdotPressed(true)
            }else if(!dotPressed && operatorPressed){
                setcurrentNumber(currentNumber+".")
                setnewNumber(newNumber+".")
                setdotPressed(true)
            }

        } 
    }

    function addOperator(operator){
        if(pastNumber !== ""){
            setcurrentNumber("")
            setoperatorPressed(true)
            setOperator(operator)
            setdotPressed(false)
        }
    }

    function getresults(){
        // eslint-disable-next-line default-case
        if (setOperator !== ""){
            // eslint-disable-next-line default-case
            switch(operator){
                case"*":
                    setcurrentNumber(parseFloat(pastNumber) * parseFloat(currentNumber))
                    setOperator("")
                    break;
                case"/":
                    setcurrentNumber(parseFloat(pastNumber) / parseFloat(currentNumber))
                    setOperator("")
                    break;
                case"+":
                    setcurrentNumber(parseFloat(pastNumber) + parseFloat(currentNumber))
                    setOperator("")
                    break;
                case"-":
                    setcurrentNumber(parseFloat(pastNumber) - parseFloat(currentNumber))
                    setOperator("")
                    break;
            }
        }
    }

    return (
        <>
            <Display content={currentNumber}/>
            <div className="button-field">
                <Button className="triple" onClick={()=> clearDisplay()}>AC</Button>  
                <Button className="colored" onClick={()=> addOperator('/')}>/</Button> 
                <Button onClick={()=> addNumber('7')}>7</Button> 
                <Button onClick={()=> addNumber('8')}>8</Button> 
                <Button onClick={()=> addNumber('9')}>9</Button> 
                <Button className="colored" onClick={()=> addOperator('*')}>*</Button> 
                <Button onClick={()=> addNumber('4')}>4</Button> 
                <Button onClick={()=> addNumber('5')}>5</Button> 
                <Button onClick={()=> addNumber('6')}>6</Button> 
                <Button className="colored" onClick={()=> addOperator('-')}>-</Button> 
                <Button onClick={()=> addNumber('1')}>1</Button> 
                <Button onClick={()=> addNumber('2')}>2</Button> 
                <Button onClick={()=> addNumber('3')}>3</Button> 
                <Button className="colored" onClick={()=> addOperator('+')}>+</Button> 
                <Button className="double" onClick={()=> addNumber('0')}>0</Button> 
                <Button onClick={()=> addDot()}>.</Button> 
                <Button className="colored" onClick={()=> getresults()}>=</Button> 
            </div>
            
        </>
    )
}

export default Calculator
